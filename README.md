# Proyecto de reorganización de los entornos web

Nombre: Cristian González Ruz

Email de La Salle: cristian.gonzalez@students.salle.url.edu

Nota:
La compilación de la imagen custom de php no funciona, sin embargo la sintaxis de los docker-compose.yml sí es correcta. Se puede comprobar con:
`docker-compose -f docker-compose.yml -f docker-compose.dev.yml config`
`docker-compose -f docker-compose.yml -f docker-compose.prod.yml config`

## Desarrollo

Cómo provisionar/levantar todo el entorno de desarrollo con un solo comando: `docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d`

Cómo desmontar todo el entorno de desarrollo con un comando: `docker-compose down -p development`

Explicar cómo ejecutar un comando de PHP situado en src/command.php: `docker-compose exec php src/command.php` `docker-compose exec web sh`

## Producción

Cómo provisionar/levantar todo el entorno de producción con un solo comando: `docker-compose up -f docker-compose.yml -f docker-compose.prod.yml -d`

Cómo desmontar todo el entorno de producción con un comando: `docker-compose down -p production`

Cómo se han evitado las colisiones del entorno de desarrollo con el de producción al ejecutarlo en una misma máquina: se usaría la opción `-p production` para producción y `-p development` para desarrollo

## Otros comandos útiles

`docker-compose logs`

`docker-compose logs service-name`